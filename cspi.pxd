cdef extern from "cspi/spi.h":
	ctypedef struct Accessible
	ctypedef Accessible AccessibleApplication
	ctypedef Accessible AccessibleAction
	ctypedef Accessible AccessibleText
	ctypedef Accessible AccessibleEditableText
	ctypedef Accessible AccessibleHypertext
	ctypedef Accessible AccessibleHyperlink
	ctypedef Accessible AccessibleImage
	ctypedef Accessible AccessibleValue
	ctypedef Accessible AccessibleSelection
	ctypedef Accessible AccessibleTable
	ctypedef Accessible AccessibleComponent

	ctypedef struct AccessibleTextRange
	ctypedef struct AccessibleKeySet
	ctypedef unsigned int SPIBoolean

	ctypedef struct AccessibleEvent:
		char  *type
		Accessible  *source
		long         detail1
		long         detail2

	ctypedef void AccessibleEventListener
	ctypedef void AccessibleKeystrokeListener
	ctypedef void AccessibleDeviceListener
	ctypedef void (*AccessibleEventListenerCB) (AccessibleEvent *event, void *user_data) except *

	ctypedef enum AccessibleDeviceEventType:
		SPI_KEY_PRESSED  = 1<<0,
		SPI_KEY_RELEASED = 1<<1,
		SPI_BUTTON_PRESSED = 1<<2,
		SPI_BUTTON_RELEASED = 1<<3
	
	ctypedef AccessibleDeviceEventType AccessibleKeyEventType

	ctypedef struct AccessibleDeviceEvent:
		long                   keyID
		short                  keycode
		char *                 keystring
		long                   timestamp
		AccessibleDeviceEventType type
		unsigned short         modifiers
		SPIBoolean             is_text


	ctypedef AccessibleDeviceEvent AccessibleKeystroke

	ctypedef SPIBoolean (*AccessibleDeviceListenerCB) (AccessibleDeviceEvent *stroke, void *user_data) except 1
	ctypedef SPIBoolean (*AccessibleKeystrokeListenerCB) (AccessibleKeystroke *stroke, void *user_data) except 1

	ctypedef unsigned long AccessibleDeviceEventMask

	ctypedef enum AccessibleCoordType:
		SPI_COORD_TYPE_SCREEN = 0,
		SPI_COORD_TYPE_WINDOW

	ctypedef enum AccessibleRole:
		SPI_ROLE_INVALID = 0,
		SPI_ROLE_ACCEL_LABEL,
		SPI_ROLE_ALERT,
		SPI_ROLE_ANIMATION,
		SPI_ROLE_ARROW,
		SPI_ROLE_CALENDAR,
		SPI_ROLE_CANVAS,
		SPI_ROLE_CHECK_BOX,
		SPI_ROLE_CHECK_MENU_ITEM,
		SPI_ROLE_COLOR_CHOOSER,
		SPI_ROLE_COLUMN_HEADER,
		SPI_ROLE_COMBO_BOX,
		SPI_ROLE_DATE_EDITOR,
		SPI_ROLE_DESKTOP_ICON,
		SPI_ROLE_DESKTOP_FRAME,
		SPI_ROLE_DIAL,
		SPI_ROLE_DIALOG,
		SPI_ROLE_DIRECTORY_PANE,
		SPI_ROLE_DRAWING_AREA,
		SPI_ROLE_FILE_CHOOSER,
		SPI_ROLE_FILLER,
		SPI_ROLE_FONT_CHOOSER,
		SPI_ROLE_FRAME,
		SPI_ROLE_GLASS_PANE,
		SPI_ROLE_HTML_CONTAINER,
		SPI_ROLE_ICON,
		SPI_ROLE_IMAGE,
		SPI_ROLE_INTERNAL_FRAME,
		SPI_ROLE_LABEL,
		SPI_ROLE_LAYERED_PANE,
		SPI_ROLE_LIST,
		SPI_ROLE_LIST_ITEM,
		SPI_ROLE_MENU,
		SPI_ROLE_MENU_BAR,
		SPI_ROLE_MENU_ITEM,
		SPI_ROLE_OPTION_PANE,
		SPI_ROLE_PAGE_TAB,
		SPI_ROLE_PAGE_TAB_LIST,
		SPI_ROLE_PANEL,
		SPI_ROLE_PASSWORD_TEXT,
		SPI_ROLE_POPUP_MENU,
		SPI_ROLE_PROGRESS_BAR,
		SPI_ROLE_PUSH_BUTTON,
		SPI_ROLE_RADIO_BUTTON,
		SPI_ROLE_RADIO_MENU_ITEM,
		SPI_ROLE_ROOT_PANE,
		SPI_ROLE_ROW_HEADER,
		SPI_ROLE_SCROLL_BAR,
		SPI_ROLE_SCROLL_PANE,
		SPI_ROLE_SEPARATOR,
		SPI_ROLE_SLIDER,
		SPI_ROLE_SPIN_BUTTON,
		SPI_ROLE_SPLIT_PANE,
		SPI_ROLE_STATUS_BAR,
		SPI_ROLE_TABLE,
		SPI_ROLE_TABLE_CELL,
		SPI_ROLE_TABLE_COLUMN_HEADER,
		SPI_ROLE_TABLE_ROW_HEADER,
		SPI_ROLE_TEAROFF_MENU_ITEM,
		SPI_ROLE_TERMINAL,
		SPI_ROLE_TEXT,
		SPI_ROLE_TOGGLE_BUTTON,
		SPI_ROLE_TOOL_BAR,
		SPI_ROLE_TOOL_TIP,
		SPI_ROLE_TREE,
		SPI_ROLE_TREE_TABLE,
		SPI_ROLE_UNKNOWN,
		SPI_ROLE_VIEWPORT,
		SPI_ROLE_WINDOW,
		SPI_ROLE_EXTENDED,
		SPI_ROLE_HEADER,
		SPI_ROLE_FOOTER,
		SPI_ROLE_PARAGRAPH,
		SPI_ROLE_RULER,
		SPI_ROLE_APPLICATION,
		SPI_ROLE_AUTOCOMPLETE,
		SPI_ROLE_EDITBAR,
		SPI_ROLE_EMBEDDED,
		SPI_ROLE_LAST_DEFINED

	ctypedef enum AccessibleState:
		SPI_STATE_INVALID = 0,
		SPI_STATE_ACTIVE,
		SPI_STATE_ARMED,
		SPI_STATE_BUSY,
		SPI_STATE_CHECKED,
		SPI_STATE_COLLAPSED,
		SPI_STATE_DEFUNCT,
		SPI_STATE_EDITABLE,
		SPI_STATE_ENABLED,
		SPI_STATE_EXPANDABLE,
		SPI_STATE_EXPANDED,
		SPI_STATE_FOCUSABLE,
		SPI_STATE_FOCUSED,
		SPI_STATE_HORIZONTAL,
		SPI_STATE_ICONIFIED,
		SPI_STATE_MODAL,
		SPI_STATE_MULTI_LINE,
		SPI_STATE_MULTISELECTABLE,
		SPI_STATE_OPAQUE,
		SPI_STATE_PRESSED,
		SPI_STATE_RESIZABLE,
		SPI_STATE_SELECTABLE,
		SPI_STATE_SELECTED,
		SPI_STATE_SENSITIVE,
		SPI_STATE_SHOWING,
		SPI_STATE_SINGLE_LINE,
		SPI_STATE_STALE,
		SPI_STATE_TRANSIENT,
		SPI_STATE_VERTICAL,
		SPI_STATE_VISIBLE,
		SPI_STATE_MANAGES_DESCENDANTS, 
		SPI_STATE_INDETERMINATE
 
	ctypedef enum AccessibleRelationType:
		SPI_RELATION_NULL,
		SPI_RELATION_LABEL_FOR,
		SPI_RELATION_LABELED_BY,
		SPI_RELATION_CONTROLLER_FOR,
		SPI_RELATION_CONTROLLED_BY,
		SPI_RELATION_MEMBER_OF,
		SPI_RELATION_NODE_CHILD_OF,
		SPI_RELATION_EXTENDED,
		SPI_RELATION_FLOWS_TO,
		SPI_RELATION_FLOWS_FROM,
		SPI_RELATION_SUBWINDOW_OF,
		SPI_RELATION_EMBEDS,
		SPI_RELATION_EMBEDDED_BY,
		SPI_RELATION_POPUP_FOR,
		SPI_RELATION_LAST_DEFINED

	ctypedef enum AccessibleKeySynthType:
		SPI_KEY_PRESS,
		SPI_KEY_RELEASE, 
		SPI_KEY_PRESSRELEASE,
		SPI_KEY_SYM,
		SPI_KEY_STRING
	
	ctypedef enum AccessibleKeyListenerSyncType:
		SPI_KEYLISTENER_NOSYNC,
		SPI_KEYLISTENER_SYNCHRONOUS,
		SPI_KEYLISTENER_CANCONSUME,
		SPI_KEYLISTENER_ALL_WINDOWS
	
	ctypedef enum Accessibility_ModifierType:
		Accessibility_MODIFIER_SHIFT,
		Accessibility_MODIFIER_SHIFTLOCK,
		Accessibility_MODIFIER_CONTROL,
		Accessibility_MODIFIER_ALT,
		Accessibility_MODIFIER_META,
		Accessibility_MODIFIER_META2,
		Accessibility_MODIFIER_META3,
		Accessibility_MODIFIER_NUMLOCK
	
	ctypedef unsigned long AccessibleModifierMaskType

	ctypedef AccessibleModifierMaskType AccessibleKeyMaskType

	ctypedef unsigned long AccessibleKeyEventMask

	ctypedef struct GArray:
		char *data
		unsigned int len

	ctypedef struct AccessibleStateSet:
		unsigned int ref_count,
		GArray *states

	ctypedef struct AccessibleRelation

	ctypedef struct SPIException

	ctypedef enum SPIExceptionCode:
		SPI_EXCEPTION_UNSPECIFIED,
		SPI_EXCEPTION_DISCONNECT,
		SPI_EXCEPTION_NO_IMPL,
		SPI_EXCEPTION_IO,
		SPI_EXCEPTION_BAD_DATA

	ctypedef enum SPIExceptionType:
		SPI_EXCEPTION_SOURCE_UNSPECIFIED,
		SPI_EXCEPTION_SOURCE_ACCESSIBLE,
		SPI_EXCEPTION_SOURCE_REGISTRY,
		SPI_EXCEPTION_SOURCE_DEVICE

	ctypedef enum AccessibleComponentLayer:
		SPI_LAYER_INVALID,
		SPI_LAYER_BACKGROUND,
		SPI_LAYER_CANVAS,
		SPI_LAYER_WIDGET,
		SPI_LAYER_MDI,
		SPI_LAYER_POPUP,
		SPI_LAYER_OVERLAY,
		SPI_LAYER_WINDOW,
		SPI_LAYER_LAST_DEFINED	

	# More information on AT-SPI C Bindings Reference Manual can be found at:
	# http://developer.gnome.org/doc/API/2.0/at-spi/index.html
 
	ctypedef SPIBoolean (* SPIExceptionHandler) (SPIException *err, SPIBoolean is_fatal) except *

	# SPI main loop and initialization
	int SPI_init () except *
	int SPI_exit () except *
	void SPI_event_main () except *
	void SPI_event_quit () except *
	void SPI_freeString                     (char *s) except *
	SPIBoolean SPI_exceptionHandlerPush (SPIExceptionHandler *handler) except *
	SPIExceptionType SPIException_getSourceType (SPIException *err) except *
	SPIExceptionCode SPIException_getExceptionCode (SPIException *err) except *
	Accessible* SPIAccessibleException_getSource (SPIException *err) except *
	char* SPIException_getDescription (SPIException *err) except *

	#AccessibleRelation
	void        AccessibleRelation_ref          (AccessibleRelation *obj) except *
	void        AccessibleRelation_unref        (AccessibleRelation *obj) except *
	int         AccessibleRelation_getNTargets  (AccessibleRelation *obj) except *
	Accessible *AccessibleRelation_getTarget    (AccessibleRelation *obj,
        	                                     int i) except *
	AccessibleRelationType AccessibleRelation_getRelationType (AccessibleRelation *obj) except *

	# Kindof wrong to add these here:
	void free (void *data)
	int strlen (char *string)

	# Accessible Objects
	void Accessible_ref                     (Accessible *obj) except *
	void Accessible_unref                   (Accessible *obj) except *
	char *Accessible_getName                (Accessible *obj) except *
	char *Accessible_getDescription         (Accessible *obj) except *
	Accessible *Accessible_getParent        (Accessible *obj) except *
	long Accessible_getChildCount           (Accessible *obj) except *
	Accessible *Accessible_getChildAtIndex  (Accessible *obj, long int i) except *
	long Accessible_getIndexInParent        (Accessible *obj) except *
	AccessibleRelation **Accessible_getRelationSet (Accessible *obj) except *

	# Accessible Objects
	SPIBoolean Accessible_isAction          (Accessible *obj) except *
	AccessibleAction *Accessible_getAction  (Accessible *obj) except *
	# Subinterface Methods - AccessibleAction Interface
	void AccessibleAction_ref               (AccessibleAction *obj) except *
	void AccessibleAction_unref             (AccessibleAction *obj) except *
	long AccessibleAction_getNActions       (AccessibleAction *obj) except *
	SPIBoolean AccessibleAction_doAction    (AccessibleAction *obj, long int i) except *
	char *AccessibleAction_getKeyBinding    (AccessibleAction *obj, long int i) except *
	char *AccessibleAction_getName          (AccessibleAction *obj, long int i) except *
	char *AccessibleAction_getDescription   (AccessibleAction *obj, long int i) except *

	AccessibleStateSet *Accessible_getStateSet     (Accessible *obj) except *
	void AccessibleStateSet_ref                    (AccessibleStateSet *obj) except *
	void AccessibleStateSet_unref                  (AccessibleStateSet *obj) except *
	void AccessibleStateSet_add                    (AccessibleStateSet *obj,
	                                                AccessibleState state) except *
	AccessibleStateSet *AccessibleStateSet_compare (AccessibleStateSet *obj,
	                                                AccessibleStateSet *obj2) except *
	SPIBoolean AccessibleStateSet_contains         (AccessibleStateSet *obj,
	                                                AccessibleState state) except *
	SPIBoolean AccessibleStateSet_equals           (AccessibleStateSet *obj,
	                                                AccessibleStateSet *obj2) except *
	SPIBoolean AccessibleStateSet_isEmpty          (AccessibleStateSet *obj)
	void AccessibleStateSet_remove                 (AccessibleStateSet *obj,
	                                                AccessibleState state) except *

	# Accessible Objects
	SPIBoolean Accessible_isText            (Accessible *obj) except *
	AccessibleText *Accessible_getText      (Accessible *obj) except *
	# Subinterface Methods - AccessibleText Interface
	void AccessibleText_ref                 (AccessibleText *obj) except *
	void AccessibleText_unref               (AccessibleText *obj) except *
	SPIBoolean  AccessibleText_addSelection     (AccessibleText *obj,
												long int startOffset,
												long int endOffset) except *
	char*       AccessibleText_getAttributes    (AccessibleText *obj,
												long int offset,
												long int *startOffset,
												long int *endOffset) except *
## pyrex doesn't like this for some reason :(
#	AccessibleTextRange** AccessibleText_getBoundedRanges
#												(AccessibleText *obj,
# 												long int x,
# 												long int y,
# 												long int width,
# 												long int height,
# 												AccessibleCoordType type,
# 												AccessibleTextClipType clipTypeX,
# 												AccessibleTextClipType clipTypeY) except *
	long        AccessibleText_getCaretOffset   (AccessibleText *obj) except *
	long AccessibleText_getCharacterCount (AccessibleText *obj) except *
## pyrex doesn't like this for some reason :(
#	void        AccessibleText_getCharacterExtents
#												(AccessibleText *obj,
# 												long int offset,
#												long int *x,
#												long int *y,
#												long int *width,
#												long int *height,
#												AccessibleCoordType type) except *
	long        AccessibleText_getNSelections   (AccessibleText *obj) except *
## pyrex doesn't like these for some reason :(
#	long        AccessibleText_getOffsetAtPoint (AccessibleText *obj,
# 												long int x,
# 												long int y,
# 												AccessibleCoordType type) except *
#	void        AccessibleText_getRangeExtents  (AccessibleText *obj,
# 												long int startOffset,
# 												long int endOffset,
# 												long int *x,
# 												long int *y,
# 												long int *width,
# 												long int *height,
# 												AccessibleCoordType type) except *
	void        AccessibleText_getSelection     (AccessibleText *obj,
												long int selectionNum,
												long int *startOffset,
												long int *endOffset) except *
	char *AccessibleText_getText (AccessibleText *obj, long int o, long int e) except *
## pyrex doesn't like these for some reason :(
#	char* AccessibleText_getTextBeforeOffset	(AccessibleText *obj,
#												long int offset,
#												AccessibleTextBoundaryType type,
#												long int *startOffset,
#												long int *endOffset) except *
#	char*       AccessibleText_getTextAfterOffset
#												(AccessibleText *obj,
#												long int offset,
#												AccessibleTextBoundaryType type,
#												long int *startOffset,
#												long int *endOffset) except *
#	char*       AccessibleText_getTextAtOffset  (AccessibleText *obj,
#												long int offset,
#												AccessibleTextBoundaryType type,
#												long int *startOffset,
#												long int *endOffset) except *
	SPIBoolean AccessibleText_removeSelection  (AccessibleText *obj,
												long int selectionNum) except *
	SPIBoolean AccessibleText_setCaretOffset   (AccessibleText *obj,
												long int newOffset) except *
	SPIBoolean AccessibleText_setSelection     (AccessibleText *obj,
												long int selectionNum,
												long int startOffset,
												long int endOffset) except *
	void        AccessibleTextRange_freeRanges  (AccessibleTextRange **ranges) except *

	# Accessible Objects
	SPIBoolean Accessible_isEditableText               (Accessible *obj) except *
	AccessibleEditableText *Accessible_getEditableText (Accessible *obj) except *
	# Subinterface Methods - AccessibleEditableText Interface
	void AccessibleEditableText_ref             (AccessibleEditableText *obj) except *
	void AccessibleEditableText_unref           (AccessibleEditableText *obj) except *
	SPIBoolean AccessibleEditableText_copyText (AccessibleText *obj,
												long int startPos,
												long int endPos) except *
	SPIBoolean AccessibleEditableText_deleteText (AccessibleEditableText *obj,
												   long int startPos,
												   long int endPos) except *

	SPIBoolean  AccessibleEditableText_insertText (AccessibleEditableText *obj,
						       long int position,
						       char *text,
						       long int length) except *
	
	SPIBoolean AccessibleEditableText_cutText  (AccessibleEditableText *obj,
 												long int startPos,
 												long int endPos) except *
	SPIBoolean AccessibleEditableText_pasteText  (AccessibleEditableText *obj,
												  long int position) except *
## pyrex doesn't like these for some reason :(
	SPIBoolean AccessibleEditableText_setTextContents (AccessibleEditableText *obj, char *newContents) except *
	SPIBoolean AccessibleEditableText_setAttributes (AccessibleEditableText *obj, char *attributes, long int startOffset, long int endOffset) except *

	# Accessible Objects
	SPIBoolean Accessible_isHypertext               (Accessible *obj) except *
	AccessibleHypertext* Accessible_getHypertext    (Accessible *obj) except *
	# Subinterface Methods - AccessibleHypertext Interface
	void AccessibleHypertext_ref             (AccessibleHypertext *obj) except *
	void AccessibleHypertext_unref           (AccessibleHypertext *obj) except *
	long AccessibleHypertext_getNLinks (AccessibleHypertext *obj) except *
	AccessibleHyperlink* AccessibleHypertext_getLink (AccessibleHypertext *obj,
												long int linkIndex) except *
	long AccessibleHypertext_getLinkIndex (AccessibleHypertext *obj,
												long int characterOffset) except *

	# Accessible Objects
	SPIBoolean Accessible_isHyperlink               (Accessible *obj) except *
	# Subinterface Methods - AccessibleHyperlink Interface
	void AccessibleHyperlink_ref             (AccessibleHyperlink *obj) except *
	void AccessibleHyperlink_unref           (AccessibleHyperlink *obj) except *
	long AccessibleHyperlink_getNAnchors (AccessibleHyperlink *obj) except *
	void AccessibleHyperlink_getIndexRange (AccessibleHyperlink *obj,
												long int *startIndex, 
												long int *endIndex) except *
	Accessible *AccessibleHyperlink_getObject (AccessibleHyperlink *obj,
												long int i) except *
	char* AccessibleHyperlink_getURI (AccessibleHyperlink *obj,
												long int i) except *
	SPIBoolean AccessibleHyperlink_isValid (AccessibleHyperlink *obj) except *

	# Accessible Objects
	SPIBoolean Accessible_isValue            (Accessible *obj) except *
	AccessibleValue *Accessible_getValue     (Accessible *obj) except *
	# Subinterface Methods - AccessibleValue Interface
	void AccessibleValue_ref                 (AccessibleValue *obj) except *
	void AccessibleValue_unref               (AccessibleValue *obj) except *
	double     AccessibleValue_getMinimumValue   (AccessibleValue *obj) except *
	double     AccessibleValue_getCurrentValue   (AccessibleValue *obj) except *
	double     AccessibleValue_getMaximumValue   (AccessibleValue *obj) except *
	double     AccessibleValue_setCurrentValue   (AccessibleValue *obj,
												double newValue) except *

	# Accessible Objects
	SPIBoolean Accessible_isImage            (Accessible *obj) except *
	AccessibleImage *Accessible_getImage     (Accessible *obj) except *
	# Subinterface Methods - AccessibleImage Interface
	void AccessibleImage_ref                 (AccessibleImage *obj) except *
	void AccessibleImage_unref               (AccessibleImage *obj) except *
	char*       AccessibleImage_getImageDescription (AccessibleImage *obj) except *
	void        AccessibleImage_getImageSize   (AccessibleImage *obj,
 												long int *width,
												long int *height) except *
	void        AccessibleImage_getImagePosition (AccessibleImage *obj,
 												  long int *x,
 												  long int *y,
 												  AccessibleCoordType ctype) except *
	void        AccessibleImage_getImageExtents (AccessibleImage *obj,
												 long int *x,
												 long int *y,
												 long int *width,
												 long int *height,
												 AccessibleCoordType ctype) except *

	# Accessible Objects
	SPIBoolean Accessible_isSelection            (Accessible *obj) except *
	AccessibleSelection *Accessible_getSelection     (Accessible *obj) except *
	# Subinterface Methods - AccessibleSelection Interface
	void AccessibleSelection_ref                 (AccessibleSelection *obj) except *
	void AccessibleSelection_unref               (AccessibleSelection *obj) except *
	int         AccessibleSelection_getNSelectedChildren (AccessibleSelection *obj) except *
	Accessible*  AccessibleSelection_getSelectedChild (AccessibleSelection *obj,
													  int i) except *
	SPIBoolean  AccessibleSelection_selectChild (AccessibleSelection *obj,
												  int i) except *
	SPIBoolean  AccessibleSelection_deselectSelectedChild (AccessibleSelection *obj,
														   int i) except *
	SPIBoolean  AccessibleSelection_isChildSelected (AccessibleSelection *obj,
												     int i) except *
	SPIBoolean  AccessibleSelection_selectAll (AccessibleSelection *obj) except *
	SPIBoolean  AccessibleSelection_clearSelection (AccessibleSelection *obj) except *

	# Accessible Objects
	SPIBoolean Accessible_isTable            (Accessible *obj) except *
	AccessibleTable *Accesssible_getTable    (Accessible *obj) except *
	# Subinterface Methods - AccessibleTable Interface
	void AccessibleTable_ref                 (AccessibleTable *obj) except *
	void AccessibleTable_unref               (AccessibleTable *obj) except *
	Accessible* AccessibleTable_getAccessibleAt (AccessibleTable *obj,
 												long int row,
 												long int column) except *
	Accessible* AccessibleTable_getCaption      (AccessibleTable *obj) except *
	long        AccessibleTable_getColumnAtIndex (AccessibleTable *obj,
 												  long int index) except *
	char*       AccessibleTable_getColumnDescription (AccessibleTable *obj,
													  long int column) except *
	long        AccessibleTable_getColumnExtentAt (AccessibleTable *obj,
												   long int row,
 												   long int column) except *
	Accessible* AccessibleTable_getColumnHeader (AccessibleTable *obj,
 												long int column) except *
	long        AccessibleTable_getIndexAt      (AccessibleTable *obj,
 												long int row,
 												long int column) except *
	long AccessibleTable_getNColumns         (AccessibleTable *obj) except *
	long AccessibleTable_getNRows            (AccessibleTable *obj) except *
	long AccessibleTable_getNSelectedColumns (AccessibleTable *obj) except *
	long AccessibleTable_getNSelectedRows    (AccessibleTable *obj) except *
	long        AccessibleTable_getRowAtIndex   (AccessibleTable *obj,
 												long int index) except *
	char*       AccessibleTable_getRowDescription (AccessibleTable *obj,
												   long int row) except *
	long        AccessibleTable_getRowExtentAt  (AccessibleTable *obj,
 												 long int row,
 												 long int column) except *
	Accessible* AccessibleTable_getRowHeader    (AccessibleTable *obj,
 												 long int row) except *
	long AccessibleTable_getSelectedRows		(AccessibleTable *obj,
												 long int **selectedRows) except *
	long AccessibleTable_getSelectedColumns		(AccessibleTable *obj,
												 long int **selectedColumns) except *
	Accessible* AccessibleTable_getSummary      (AccessibleTable *obj) except *
	SPIBoolean  AccessibleTable_isColumnSelected (AccessibleTable *obj,
 												  long int column) except *
	SPIBoolean  AccessibleTable_isRowSelected   (AccessibleTable *obj,
 												 long int row) except *
	SPIBoolean  AccessibleTable_isSelected      (AccessibleTable *obj,
 												 long int row,
 												 long int column) except *

	# AccessibleApplication Methods - AccessibleApplication API
	void AccessibleApplication_ref               (AccessibleApplication *obj) except *
	void AccessibleApplication_unref             (AccessibleApplication *obj) except *
	char *AccessibleApplication_getToolkitName (AccessibleApplication *obj) except *
	char*      AccessibleApplication_getVersion (AccessibleApplication *obj) except *
	long       AccessibleApplication_getID      (AccessibleApplication *obj) except *
	SPIBoolean AccessibleApplication_pause      (AccessibleApplication *obj) except *
	SPIBoolean AccessibleApplication_resume     (AccessibleApplication *obj) except *

	# Accessible Objects
	SPIBoolean Accessible_isComponent (Accessible *obj) except *
	AccessibleComponent *Accessible_getComponent (Accessible *obj) except *
	Accessible *AccessibleComponent_getAccessibleAtPoint (
			AccessibleComponent *obj,
			long int             x,
			long int             y,
			AccessibleCoordType  ctype) except *
	void        AccessibleComponent_getExtents  (AccessibleComponent *obj,
			long int            *x,
			long int            *y,
			long int            *width,
			long int            *height,
			AccessibleCoordType  ctype) except *
	void        AccessibleComponent_getPosition (AccessibleComponent *obj,
			long int            *x,
			long int            *y,
			AccessibleCoordType  ctype) except *
	void        AccessibleComponent_getSize     (AccessibleComponent *obj,
			long int            *width,
			long int            *height) except *
	AccessibleComponentLayer	AccessibleComponent_getLayer	(AccessibleComponent *obj) except *
	SPIBoolean  AccessibleComponent_grabFocus   (AccessibleComponent *obj) except *
	short       AccessibleComponent_getMDIZOrder(AccessibleComponent *obj) except *

	# Accessible Objects
	SPIBoolean Accessible_isApplication (Accessible *obj) except *
	AccessibleApplication *Accessible_getApplication (Accessible *obj) except *
	AccessibleRole Accessible_getRole       (Accessible *obj) except *
	char* Accessible_getRoleName			(Accessible *obj) except *
	char *AccessibleRole_getName            (AccessibleRole role) except *

	# Registry queries
	int SPI_getDesktopCount                 () except *
	Accessible *SPI_getDesktop              (int i) except *
	int SPI_getDesktopList                  (Accessible ***desktop_list) except *

	# Synthesizing events:
	SPIBoolean SPI_generateMouseEvent (long x, long y, char *name) except *
	SPIBoolean SPI_generateKeyboardEvent (long int keyval,
	                                      char *keystring,
                                              AccessibleKeySynthType synth_type) except *

	SPIBoolean SPI_registerGlobalEventListener (AccessibleEventListener *listener, char *eventType) except *
	SPIBoolean SPI_deregisterGlobalEventListener (AccessibleEventListener *listener, char *eventType) except *

	# Event Listener Support
	SPIBoolean AccessibleEvent_ref (AccessibleEvent *event) except *
	void AccessibleEvent_unref (AccessibleEvent *event) except *
	AccessibleEventListener* SPI_createAccessibleEventListener (AccessibleEventListenerCB callback, void *user_data) except *
	void AccessibleEventListener_unref (AccessibleEventListener *listener) except *
	SPIBoolean AccessibleEventListener_addCallback (AccessibleEventListener *listener, AccessibleEventListenerCB callback, void *user_data) except *
	SPIBoolean AccessibleEventListener_removeCallback (AccessibleEventListener *listener, AccessibleEventListenerCB callback) except *
	Accessible* AccessibleActiveDescendantChangedEvent_getActiveDescendant (AccessibleEvent *event) except *
	Accessible* AccessibleChildChangedEvent_getChildAccessible (AccessibleEvent *event) except *
	char* AccessibleDescriptionChangedEvent_getDescriptionString (AccessibleEvent *event) except *
	char* AccessibleNameChangedEvent_getNameString (AccessibleEvent *event) except *
	Accessible* AccessibleParentChangedEvent_getParentAccessible (AccessibleEvent *event) except *
	char* AccessibleTableCaptionChangedEvent_getCaptionString (AccessibleEvent *event) except *
	char* AccessibleTableColumnDescriptionChangedEvent_getDescriptionString (AccessibleEvent *event) except *
	Accessible* AccessibleTableHeaderChangedEvent_getHeaderAccessible (AccessibleEvent *event) except *
	char* AccessibleTableRowDescriptionChangedEvent_getDescriptionString (AccessibleEvent *event) except *
	Accessible* AccessibleTableSummaryChangedEvent_getSummaryAccessible (AccessibleEvent *event) except *
	char* AccessibleTextChangedEvent_getChangeString (AccessibleEvent *event) except *
	char* AccessibleTextSelectionChangedEvent_getSelectionString (AccessibleEvent *event) except *
	char* AccessibleWindowEvent_getTitleString (AccessibleEvent *event) except *

	# Registry Queries for AccessibleKeystrokeListener
	SPIBoolean SPI_registerAccessibleKeystrokeListener (AccessibleKeystrokeListener *listener, AccessibleKeySet *keys, AccessibleKeyMaskType modmask, AccessibleKeyEventMask eventmask, AccessibleKeyListenerSyncType sync_type) except *
	SPIBoolean SPI_deregisterAccessibleKeystrokeListener (AccessibleKeystrokeListener *listener, AccessibleKeyMaskType modmask) except *

	# AccessibleKeystrokeListener Support
	AccessibleKeystrokeListener * SPI_createAccessibleKeystrokeListener (AccessibleKeystrokeListenerCB callback, void *user_data) except *
	SPIBoolean AccessibleKeystrokeListener_addCallback (AccessibleKeystrokeListener *listener, AccessibleKeystrokeListenerCB callback, void *user_data) except *
	SPIBoolean AccessibleKeystrokeListener_removeCallback (AccessibleKeystrokeListener *listener, AccessibleKeystrokeListenerCB callback) except *
	void AccessibleKeystrokeListener_unref (AccessibleKeystrokeListener *listener) except *

	# Registry Queries for DeviceEventListener
	AccessibleDeviceListener* SPI_createAccessibleDeviceListener (AccessibleDeviceListenerCB callback, void *user_data) except *
	SPIBoolean SPI_registerDeviceEventListener (AccessibleDeviceListener *listener, AccessibleDeviceEventMask eventmask, void *filter) except *
	SPIBoolean SPI_deregisterDeviceEventListener (AccessibleDeviceListener *listener, void *filter) except *


	# DeviceEventListener Support
	SPIBoolean AccessibleDeviceListener_addCallback (AccessibleDeviceListener *listener, AccessibleDeviceListenerCB callback, void *user_data) except *
	SPIBoolean AccessibleDeviceListener_removeCallback (AccessibleDeviceListener *listener, AccessibleDeviceListenerCB callback) except *
	void AccessibleDeviceListener_unref (AccessibleDeviceListener *listener) except *


# vim: sw=4 ts=4 sts=4 noet ai
