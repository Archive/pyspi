cdef extern from "X11/Xlib.h":
    ctypedef struct Display
    ctypedef unsigned long KeySym
    ctypedef unsigned char KeyCode
    Display *XOpenDisplay(char*)
    int XCloseDisplay(Display*)
    KeySym XStringToKeysym(char*)
    KeyCode XKeysymToKeycode(Display*, KeySym)

