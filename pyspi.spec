Summary: Python bindings for AT-SPI
Name: pyspi
Version: 0.6.1
Release: 1%{?dist}
License: LGPL
Group: Development/Languages
URL: http://people.redhat.com/zcerza/dogtail/
Source0: http://people.redhat.com/zcerza/dogtail/releases/pyspi-%{version}.tar.gz
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: at-spi-devel, Pyrex, libX11-devel

%description
AT-SPI allows assistive technologies to access GTK-based applications.  It
exposes the internals of applications for automation, so tools such as screen
readers and scripting interfaces can query and interact with GUI controls.

pyspi allows the Python language to be used to script AT-SPI-aware 
applications (currently mostly GTK+ based.)

%prep
%setup -q

%build
python ./setup.py build

%install
rm -rf $RPM_BUILD_ROOT
python ./setup.py install -O2 --root=$RPM_BUILD_ROOT --record=%{name}.files

%clean
rm -rf $RPM_BUILD_ROOT

%files -f %{name}.files
%defattr(-,root,root,-)
%doc COPYING
%doc NEWS
%doc ChangeLog

%changelog
* Fri Oct 13 2006 Zack Cerza <zcerza@redhat.com> - 0.6.1-1
- New upstream release.

* Wed Sep 13 2006 Zack Cerza <zcerza@redhat.com> - 0.6.0-1
- New upstream release.
- Add libX11-devel to BuildRequires.

* Tue Aug 01 2006 Zack Cerza <zcerza@redhat.com> - 0.5.5-1
- New upstream release.

* Tue Mar 21 2006 Zack Cerza <zcerza@redhat.com> - 0.5.4-2
- Fix URL and Source0 fields.

* Fri Mar 10 2006 Zack Cerza <zcerza@redhat.com> - 0.5.4-1
- Remove all Requires
- Add package versions to this changelog
- Add new description
- Use official Fedora Extras BuildRoot
- Change the group to Development/Languages

* Tue Feb 14 2006 Zack Cerza <zcerza@redhat.com> - 0.5.3-2
- Add missing BuildRequires on Pyrex.

* Tue Feb  7 2006 Zack Cerza <zcerza@redhat.com> - 0.5.3-1
- Initial build.

