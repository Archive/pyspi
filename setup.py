#!/usr/bin/env python
__author__ = 'Zack Cerza <zcerza@redhat.com>'
version = '0.6.1'

import sys
from distutils.core import setup
from distutils.extension import Extension
try: 
	from Pyrex.Distutils import build_ext
except ImportError, inst:
	if sys.argv[1] not in ('clean', 'sdist'):
		raise ImportError, inst
	else: 
		build_ext = None
		bdist_rpm = None
from distutils.command.bdist_rpm import bdist_rpm

# All this pkg-config magic taken from:
# http://svn.xiph.org/icecast/tags/shout_python-0_1/shout-python/setup.py
# The shout-python package is LGPL.
import os
if os.system('pkg-config --exists cspi-1.0 2> /dev/null') != 0:
	print 'Error: pkg-config could not find cspi-1.0'
	print 'Install "at-spi-devel" or the equivalent package for your system'
	sys.exit(1)
elif os.system('pkg-config --exists x11 2> /dev/null') != 0:
	print 'Error: pkg-config could not find x11'
	print 'Install "libX11-devel" or the equivalent package for your system'
	sys.exit(1)
else:
	pkgcfg = os.popen('pkg-config --cflags cspi-1.0 x11')
	cflags = pkgcfg.readline().strip()
	pkgcfg.close()
	pkgcfg = os.popen('pkg-config --libs cspi-1.0 x11')
	libs = pkgcfg.readline().strip()
	pkgcfg.close()

iflags = [x[2:] for x in cflags.split() if x[0:2] == '-I']
extra_cflags = [x for x in cflags.split() if x[0:2] != '-I']
libdirs = [x[2:] for x in libs.split() if x[0:2] == '-L']
libsonly = [x[2:] for x in libs.split() if x[0:2] == '-l']
# End pkg-config magic


atspi = Extension(
	name = 'atspi', 
	sources = ['atspi.pyx'], 
#	depends = ['cspi.pxd', 'pyspi.pyx'],
	include_dirs = iflags,
	extra_compile_args = extra_cflags,
	library_dirs = libdirs,
	libraries = libsonly)

setup (
	name = 'pyspi',
	version = version,
	description = 'Python bindings for CSPI',
	url = 'http://people.redhat.com/zcerza/dogtail/',
	author = """Zack Cerza <zcerza@redhat.com>,
	Chris Lee <clee@redhat.com>,
	Lawrence Lim <llim@redhat.com>,
	David Malcolm <dmalcolm@redhat.com>""",
	author_email = 'pyspi-list@gnome.org',
	ext_modules = [atspi],
	cmdclass = {
		'build_ext': build_ext,
		'bdist_rpm': bdist_rpm
		}
)

# vim: sw=4 ts=4 sts=4 noet ai

