# Authors: Zack Cerza <zcerza@redhat.com>
#          Chris Lee  <clee@redhat.com>
# Include the C stuff
cimport cspi
cimport Xlib

# Include the Python stuff
include "pyspi.pyx"
