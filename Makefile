# pyspi *development* Makefile

all:
	python setup.py build_ext --inplace

clean:
# The following line should ideally clean everything, but it doesn't.
	python setup.py clean
#	For now we work around it.
	rm -f atspi.so atspi.o atspi.c MANIFEST
	rm -rf build dist
	
tarball:
	python setup.py sdist

rpm_prep: tarball
	mkdir -p rpms/{BUILD,RPMS,SOURCES,SPECS,SRPMS}
	# Create an rpmrc that will include our custom rpmmacros file
	echo "%_topdir `pwd`/rpms/" > rpms/tmp.rpmmacros
	echo "macrofiles: /usr/lib/rpm/macros:/usr/lib/rpm/%{_target}/macros:/usr/lib/rpm/redhat/macros:/etc/rpm/macros.*:/etc/rpm/macros:/etc/rpm/%{_target}/macros:~/.rpmmacros:`pwd`/rpms/tmp.rpmmacros" > rpms/tmp.rpmrc

rpm: rpm_prep
	# Build using the custom rpmrc in the rpms/ sub-dir
	rpmbuild --rcfile /usr/lib/rpm/rpmrc:/usr/lib/rpm/redhat/rpmrc:`pwd`/rpms/tmp.rpmrc  -tb dist/pyspi-*.tar.gz
	# Move the binary RPMs to dist/
	mv rpms/RPMS/*/* dist/
	rm -rf rpms/

srpm: rpm_prep
	# Build using the custom rpmrc in the rpms/ sub-dir
	rpmbuild --rcfile /usr/lib/rpm/rpmrc:/usr/lib/rpm/redhat/rpmrc:`pwd`/rpms/tmp.rpmrc  -ts dist/pyspi-*.tar.gz --nodeps
	# Move the source RPM to dist/
	mv rpms/SRPMS/* dist/
	rm -rf rpms/
	
deb:
	dpkg-buildpackage -rfakeroot -us -uc

install:
	python setup.py install
